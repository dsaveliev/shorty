package handlers

import (
	"database/sql"
	"net/http"

	"gitlab.com/dsaveliev/shorty/errors"
	"gitlab.com/dsaveliev/shorty/models"
	"github.com/labstack/echo"
	reform "gopkg.in/reform.v1"
)

// LinkHandler represents shortcode actions
type LinkHandler struct {
	DB *reform.DB
}

// Show action returns shortcode value
func (h *LinkHandler) Show(c echo.Context) error {
	shortCode := c.Param("shortcode")

	linkRecord, err := h.DB.FindByPrimaryKeyFrom(models.LinkTable, shortCode)
	if err == sql.ErrNoRows {
		return onError(c, errors.ErrorShortCodeNotFound)
	} else if err != nil {
		return onError(c, err)
	}

	link := linkRecord.(*models.Link)
	link.UpdateStats()

	if err := h.DB.Save(link); err != nil {
		return onError(c, err)
	}

	return c.Redirect(http.StatusFound, link.URL)
}

// Create action creates shortcode
func (h *LinkHandler) Create(c echo.Context) error {
	link := new(models.Link)
	if err := c.Bind(link); err != nil {
		return onError(c, err)
	}

	_, err := h.DB.FindByPrimaryKeyFrom(models.LinkTable, link.ShortCode)
	if err == nil {
		return onError(c, errors.ErrorShortCodeInUse)
	} else if err != sql.ErrNoRows {
		return onError(c, err)
	}

	if err := link.Validate(); err != nil {
		return onError(c, err)
	}

	if err := h.DB.Insert(link); err != nil {
		return onError(c, err)
	}

	return c.JSON(http.StatusCreated, link.ShortCodePresenter())
}

// Stats action returns shortcode statistics
func (h *LinkHandler) Stats(c echo.Context) error {
	shortCode := c.Param("shortcode")

	linkRecord, err := h.DB.FindByPrimaryKeyFrom(models.LinkTable, shortCode)
	if err == sql.ErrNoRows {
		return onError(c, errors.ErrorShortCodeNotFound)
	} else if err != nil {
		return onError(c, err)
	}

	link := linkRecord.(*models.Link)

	return c.JSON(http.StatusOK, link.StatsPresenter())
}

func onError(c echo.Context, err error) error {
	hErr, ok := err.(errors.AppError)
	if !ok {
		c.Logger().Error(err)
		hErr = errors.ErrorUnexpectedBehaviour
	}
	c.JSON(hErr.Status, hErr.ToJSON())
	return hErr
}
