package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"gitlab.com/dsaveliev/shorty/db"
	"gitlab.com/dsaveliev/shorty/errors"
	"gitlab.com/dsaveliev/shorty/models"
	"github.com/joho/godotenv"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
)

const iso8601 = "2006-01-02T15:04:05.999999Z"

var handler *LinkHandler

func loadTestEnv() {
	testDir, _ := os.Getwd()
	appPath := filepath.Dir(testDir)
	if err := godotenv.Load(appPath + "/.env.test"); err != nil {
		log.Fatalf("☹️  There is no .env.test file: %s", err)
	}
}

func init() {
	// load test env variables
	loadTestEnv()

	// initialize database connection
	db := db.Init()

	// create handler
	handler = &LinkHandler{
		DB: db,
	}
}

func TestShow(t *testing.T) {
	// create link
	link := &models.Link{
		URL: "http://example.com",
	}
	link.Validate()
	handler.DB.Insert(link)
	lastSeenDate := link.LastSeenDate
	shortCode := link.ShortCode

	// teardown
	defer func() {
		handler.DB.Delete(link)
	}()

	// setup
	e := echo.New()
	req := httptest.NewRequest(echo.GET, "/"+shortCode, nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/:shortcode")
	c.SetParamNames("shortcode")
	c.SetParamValues(shortCode)

	// assertions
	assert.NotZero(t, link.ShortCode)
	assert.NotZero(t, link.StartDate)
	assert.NotZero(t, link.LastSeenDate)
	assert.Equal(t, uint64(0), link.RedirectCount)

	if assert.NoError(t, handler.Show(c)) {
		linkRecord, _ := handler.DB.FindByPrimaryKeyFrom(models.LinkTable, shortCode)
		link = linkRecord.(*models.Link)

		assert.Equal(t, http.StatusFound, rec.Code)
		assert.Equal(t, link.URL, rec.Header()["Location"][0])
		assert.Equal(t, uint64(1), link.RedirectCount)
		assert.True(t, lastSeenDate.Before(link.LastSeenDate))
	}
}

func TestShowNotFound(t *testing.T) {
	// create link
	link := &models.Link{
		URL:           "http://example.com",
		ShortCode:     "123456",
		RedirectCount: 5,
	}
	link.Validate()
	handler.DB.Insert(link)

	// teardown
	defer func() {
		handler.DB.Delete(link)
	}()

	// setup
	e := echo.New()
	req := httptest.NewRequest(echo.GET, "/000000", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/:shortcode")
	c.SetParamNames("shortcode")
	c.SetParamValues("000000")

	// assertions
	if assert.Error(t, handler.Stats(c)) {
		actualResponse := map[string]interface{}{}
		if err := json.NewDecoder(rec.Body).Decode(&actualResponse); err != nil {
			log.Fatalln(err)
			t.Error(err)
		}
		assert.Equal(t, http.StatusNotFound, rec.Code)
		assert.Equal(t, errors.ErrorShortCodeNotFound.ToJSON(), actualResponse)
	}
}

func TestStats(t *testing.T) {
	// create link
	link := &models.Link{
		URL:           "http://example.com",
		ShortCode:     "123456",
		RedirectCount: 5,
	}
	link.Validate()
	handler.DB.Insert(link)

	// teardown
	defer func() {
		handler.DB.Delete(link)
	}()

	// setup
	e := echo.New()
	req := httptest.NewRequest(echo.GET, "/123456/stats", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/:shortcode/stats")
	c.SetParamNames("shortcode")
	c.SetParamValues("123456")

	// assertions
	if assert.NoError(t, handler.Stats(c)) {
		expectedResponse := link.StatsPresenter()
		actualResponse := map[string]interface{}{}
		if err := json.NewDecoder(rec.Body).Decode(&actualResponse); err != nil {
			log.Fatalln(err)
			t.Error(err)
		}

		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, expectedResponse["redirectCount"], uint64(actualResponse["redirectCount"].(float64)))
		assert.Equal(t, expectedResponse["startDate"].(time.Time).Format(iso8601), actualResponse["startDate"])
		assert.Equal(t, expectedResponse["lastSeenDate"].(time.Time).Format(iso8601), actualResponse["lastSeenDate"])
	}
}

func TestStatsNotFound(t *testing.T) {
	// create link
	link := &models.Link{
		URL:           "http://example.com",
		ShortCode:     "123456",
		RedirectCount: 5,
	}
	link.Validate()
	handler.DB.Insert(link)

	// teardown
	defer func() {
		handler.DB.Delete(link)
	}()

	// setup
	e := echo.New()
	req := httptest.NewRequest(echo.GET, "/000000/stats", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/:shortcode/stats")
	c.SetParamNames("shortcode")
	c.SetParamValues("000000")

	// assertions
	if assert.Error(t, handler.Stats(c)) {
		actualResponse := map[string]interface{}{}
		if err := json.NewDecoder(rec.Body).Decode(&actualResponse); err != nil {
			log.Fatalln(err)
			t.Error(err)
		}
		assert.Equal(t, http.StatusNotFound, rec.Code)
		assert.Equal(t, errors.ErrorShortCodeNotFound.ToJSON(), actualResponse)
	}
}

func TestCreate(t *testing.T) {
	linkJSON := `{
		"url": "http://example.com",
		"shortcode": "123456"
	  }`

	// setup
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/shorten", strings.NewReader(linkJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/shorten")

	// assertions
	if assert.NoError(t, handler.Create(c)) {
		linkRecord, err := handler.DB.FindByPrimaryKeyFrom(models.LinkTable, "123456")
		assert.NoError(t, err)
		link := linkRecord.(*models.Link)

		// teardown
		defer func() {
			handler.DB.Delete(link)
		}()

		expectedResponse := link.ShortCodePresenter()
		actualResponse := map[string]interface{}{}
		if err := json.NewDecoder(rec.Body).Decode(&actualResponse); err != nil {
			log.Fatalln(err)
			t.Error(err)
		}

		assert.Equal(t, http.StatusCreated, rec.Code)
		assert.Equal(t, expectedResponse, actualResponse)
	}
}

func TestCreateShortCodeInUse(t *testing.T) {
	linkJSON := `{
		"url": "http://example.com",
		"shortcode": "123456"
	  }`

	// create link
	link := &models.Link{
		URL:       "http://example.com",
		ShortCode: "123456",
	}
	link.Validate()
	handler.DB.Insert(link)

	// teardown
	defer func() {
		handler.DB.Delete(link)
	}()

	// setup
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/shorten", strings.NewReader(linkJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/shorten")

	// assertions
	if assert.Error(t, handler.Create(c)) {
		assert.Equal(t, http.StatusConflict, rec.Code)
	}
}

func TestCreateEmptyURL(t *testing.T) {
	linkJSON := `{
		"url": "",
		"shortcode": "123456"
	  }`

	// setup
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/shorten", strings.NewReader(linkJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/shorten")

	// assertions
	if assert.Error(t, handler.Create(c)) {
		assert.Equal(t, http.StatusBadRequest, rec.Code)
	}
}

func TestCreateWrongShortCodeFormat(t *testing.T) {
	linkJSON := `{
		"url": "http://example.com",
		"shortcode": "1234%^&56"
	  }`

	// setup
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/shorten", strings.NewReader(linkJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/shorten")

	// assertions
	if assert.Error(t, handler.Create(c)) {
		assert.Equal(t, http.StatusUnprocessableEntity, rec.Code)
	}
}
