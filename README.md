Shorty Challenge
================

# Solution

## Overview

Serivce is written in Golang with [Echo framework](https://echo.labstack.com/) and [PostgreSQL](https://www.postgresql.org/) as
a database.

Keep in mind, that there are some simplifications and сompromises due to the time limit and formulation the problem.

## Project structure

```
.
*** DB initialization & migrations
├── db
│   ├── db.go
│   └── migrations
*** Custom errors for the handlers
├── errors
│   └── errors.go
*** Handlers
├── handlers
│   ├── handlers.go
│   └── handlers_test.go
*** Domain models and DAO 
├── models
│   ├── models.go
│   ├── models_reform.go
│   └── models_test.go
*** Entry point
├── main.go
*** Initialization script for DB container
├── bin
│   └── createdb.sh
*** Containers and docker compose stuff
├── Dockerfile 
├── docker-compose.yml
*** Setup script for the test environment
├── install.sh 
*** Packages managemet (via vgo)
├── go.mod 
├── go.sum
*** Project description
└── README.md
```

## ORM & Database migration

A [Reform package](https://github.com/go-reform/reform) is used as a lightweight ORM. It has concise and idiomatic interface due to the code generation.

I'm using [golang-migrate package](https://github.com/golang-migrate/migrate) as a built-in migration tool. This choice was made for the sake of easy integration with docker-compose workflow.

Every time the service is running migration tool checks pending migrations. From my point of view, it's better to keep migrations outside of the code base.

## Testing

I'm using plain Golang tests with [Testify](https://github.com/stretchr/testify) as assertions toolkit.

In order to run the tests you need to follow next steps:

1. Check / edit `.env.test` file
2. Setup containers `docker-compose up`
3. Run `go test -count=1 -cover ./...`

```bash
$ go test -count=1 -cover ./...
?   	gitlab.com/dsaveliev/shorty	[no test files]
?   	gitlab.com/dsaveliev/shorty/db	[no test files]
?   	gitlab.com/dsaveliev/shorty/errors	[no test files]
ok  	gitlab.com/dsaveliev/shorty/handlers	10.083s	coverage: 76.3% of statements
ok  	gitlab.com/dsaveliev/shorty/models	0.024s	coverage: 48.8% of statements
```

# Task description

## The Challenge

The challenge, if you choose to accept it, is to create a micro service to shorten urls, in the style that TinyURL and bit.ly made popular.

## Rules

1. The service must expose HTTP endpoints according to the definition below.
2. Use [docker](https://docs.docker.com/) and [docker compose](https://docs.docker.com/compose/overview/), the only command needed to run your project must be `docker-compose up` 
3. It must be well tested, it must also be possible to run the entire test suit with a single command from the directory of your repository.
4. The service must be versioned using git and submitted by making a Pull Request against this repository, git history **should** be meaningful.
5. You don't have to use a datastore, you can have all data in memory, but we'd be more impressed if you do use one.

## Tips

* Less is more, small is beautiful, you know the drill — stick to the requirements.
* Use the right tool for the job, rails is highly discouraged.
* Don't try to make the microservice play well with others, the system is all yours.
* No need to take care of domains, that's for a reverse proxy to handle.
* Unit tests > Integration tests, but be careful with untested parts of the system.

**Good Luck!** — not that you need any ;)

-------------------------------------------------------------------------

## API Documentation

**All responses must be encoded in JSON and have the appropriate Content-Type header**


### POST /shorten

```
POST /shorten
Content-Type: "application/json"

{
  "url": "http://example.com",
  "shortcode": "example"
}
```

Attribute | Description
--------- | -----------
**url**   | url to shorten
shortcode | preferential shortcode

##### Returns:

```
201 Created
Content-Type: "application/json"

{
  "shortcode": :shortcode
}
```

A random shortcode is generated if none is requested, the generated short code has exactly 6 alpahnumeric characters and passes the following regexp: ```^[0-9a-zA-Z_]{6}$```.

##### Errors:

Error | Description
----- | ------------
400   | ```url``` is not present
409   | The the desired shortcode is already in use. **Shortcodes are case-sensitive**.
422   | The shortcode fails to meet the following regexp: ```^[0-9a-zA-Z_]{4,}$```.


### GET /:shortcode

```
GET /:shortcode
Content-Type: "application/json"
```

Attribute      | Description
-------------- | -----------
**shortcode**  | url encoded shortcode

##### Returns

**302** response with the location header pointing to the shortened URL

```
HTTP/1.1 302 Found
Location: http://www.example.com
```

##### Errors

Error | Description
----- | ------------
404   | The ```shortcode``` cannot be found in the system

### GET /:shortcode/stats

```
GET /:shortcode/stats
Content-Type: "application/json"
```

Attribute      | Description
-------------- | -----------
**shortcode**  | url encoded shortcode

##### Returns

```
200 OK
Content-Type: "application/json"

{
  "startDate": "2012-04-23T18:25:43.511Z",
  "lastSeenDate": "2012-04-23T18:25:43.511Z",
  "redirectCount": 1
}
```

Attribute         | Description
--------------    | -----------
**startDate**     | date when the url was encoded, conformant to [ISO8601](http://en.wikipedia.org/wiki/ISO_8601)
**redirectCount** | number of times the endpoint ```GET /shortcode``` was called
lastSeenDate      | date of the last time the a redirect was issued, not present if ```redirectCount == 0```

##### Errors

Error | Description
----- | ------------
404   | The ```shortcode``` cannot be found in the system


