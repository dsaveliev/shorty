package db

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/golang-migrate/migrate/v3"
	"github.com/golang-migrate/migrate/v3/database/postgres"

	// migrate drivers
	_ "github.com/golang-migrate/migrate/v3/source/file"

	// load postgres adapter
	_ "github.com/lib/pq"

	"gopkg.in/reform.v1"
	reform_psql "gopkg.in/reform.v1/dialects/postgresql"
)

// Init creates new *reform.DB instance
func Init() *reform.DB {
	connStr := fmt.Sprintf("user=%s dbname=%s host=%s sslmode=disable", os.Getenv("DB_USER"), os.Getenv("DB_NAME"), os.Getenv("DB_HOST"))

	sqlDB, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatalf("bad db connstring: %s", err)
	}

	// run migrations
	driver, err := postgres.WithInstance(sqlDB, &postgres.Config{})
	if err != nil {
		log.Fatalf("migration driver failed: %s\n", err)
	}
	m, err := migrate.NewWithDatabaseInstance(os.Getenv("DB_MIGRATIONS"), "postgres", driver)
	if err != nil {
		log.Fatalf("migration failed: %s\n", err)
	}
	m.Up()

	return reform.NewDB(sqlDB, reform_psql.Dialect, nil)
}
