CREATE TABLE link
(
   shortcode character varying NOT NULL, 
   url character varying NOT NULL, 
   redirect_count bigint NOT NULL DEFAULT 0, 
   start_date timestamp without time zone NOT NULL, 
   last_seen_date timestamp without time zone NOT NULL,
   CONSTRAINT media_pkey PRIMARY KEY (shortcode)
) 
WITH (
  OIDS = FALSE
);

CREATE UNIQUE INDEX index_link_on_code
  ON link USING btree (shortcode);