package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"gitlab.com/dsaveliev/shorty/db"
	"gitlab.com/dsaveliev/shorty/handlers"
	"github.com/joho/godotenv"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	var err error

	// seed random generator
	rand.Seed(time.Now().UnixNano())

	// load env variables
	if err = godotenv.Load(); err != nil {
		log.Fatalf("☹️  There is no .env file: %s", err)
	}

	// initialize database connection
	db := db.Init()

	// create handler
	s := handlers.LinkHandler{
		DB: db,
	}

	// create server instance
	e := echo.New()

	// setup middlewares
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// routing
	e.GET("/:shortcode", s.Show)
	e.GET("/:shortcode/stats", s.Stats)
	e.POST("/shorten", s.Create)

	// start server
	bindAddress := fmt.Sprintf(
		"%s:%s",
		os.Getenv("APP_HOST"),
		os.Getenv("APP_PORT"),
	)
	e.Logger.Fatal(e.Start(bindAddress))
}
