package errors

import (
	"net/http"
)

// AppError represents custom error for handlers
type AppError struct {
	Status  int
	Message string
}

// Error implements error interface
func (e AppError) Error() string {
	return e.Message
}

// ToJSON produces error response
func (e AppError) ToJSON() map[string]interface{} {
	return map[string]interface{}{
		"message": e.Message,
	}
}

var (
	// ErrorEmptyURL represents the case of empty URL
	ErrorEmptyURL = AppError{http.StatusBadRequest, "URL is not present"}
	// ErrorShortCodeInUse represents the case of already taken shortcode
	ErrorShortCodeInUse = AppError{http.StatusConflict, "The the desired shortcode is already in use"}
	// ErrorShortCodeNotFound represents the case of missed shortcode
	ErrorShortCodeNotFound = AppError{http.StatusNotFound, "Shortcode not found"}
	// ErrorWrongShortCodeFormat represents the case of invalid shortcode
	ErrorWrongShortCodeFormat = AppError{http.StatusUnprocessableEntity, "Wrong shortcode format"}
	// ErrorUnexpectedBehaviour represents generic problem
	ErrorUnexpectedBehaviour = AppError{http.StatusTeapot, "Server error: unexpected behaviour"}
)
