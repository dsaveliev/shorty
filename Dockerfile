FROM golang:1.11.1-alpine AS builder
RUN apk add --update git bash gcc musl-dev && rm -rf /var/cache/apk/*
WORKDIR /shorty/
ADD . .
RUN go build ./...
RUN GOGC=off go build -v -ldflags '-w -s' -o app


FROM alpine:latest AS web
RUN apk --no-cache add ca-certificates postgresql-client
WORKDIR /root/
COPY --from=builder /shorty .
CMD ["./app"]
EXPOSE 8080
