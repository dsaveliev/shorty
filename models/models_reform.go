// Code generated by gopkg.in/reform.v1. DO NOT EDIT.

package models

import (
	"fmt"
	"strings"

	"gopkg.in/reform.v1"
	"gopkg.in/reform.v1/parse"
)

type linkTableType struct {
	s parse.StructInfo
	z []interface{}
}

// Schema returns a schema name in SQL database ("").
func (v *linkTableType) Schema() string {
	return v.s.SQLSchema
}

// Name returns a view or table name in SQL database ("link").
func (v *linkTableType) Name() string {
	return v.s.SQLName
}

// Columns returns a new slice of column names for that view or table in SQL database.
func (v *linkTableType) Columns() []string {
	return []string{"shortcode", "url", "redirect_count", "start_date", "last_seen_date"}
}

// NewStruct makes a new struct for that view or table.
func (v *linkTableType) NewStruct() reform.Struct {
	return new(Link)
}

// NewRecord makes a new record for that table.
func (v *linkTableType) NewRecord() reform.Record {
	return new(Link)
}

// PKColumnIndex returns an index of primary key column for that table in SQL database.
func (v *linkTableType) PKColumnIndex() uint {
	return uint(v.s.PKFieldIndex)
}

// LinkTable represents link view or table in SQL database.
var LinkTable = &linkTableType{
	s: parse.StructInfo{Type: "Link", SQLSchema: "", SQLName: "link", Fields: []parse.FieldInfo{{Name: "ShortCode", Type: "string", Column: "shortcode"}, {Name: "URL", Type: "string", Column: "url"}, {Name: "RedirectCount", Type: "uint64", Column: "redirect_count"}, {Name: "StartDate", Type: "time.Time", Column: "start_date"}, {Name: "LastSeenDate", Type: "time.Time", Column: "last_seen_date"}}, PKFieldIndex: 0},
	z: new(Link).Values(),
}

// String returns a string representation of this struct or record.
func (s Link) String() string {
	res := make([]string, 5)
	res[0] = "ShortCode: " + reform.Inspect(s.ShortCode, true)
	res[1] = "URL: " + reform.Inspect(s.URL, true)
	res[2] = "RedirectCount: " + reform.Inspect(s.RedirectCount, true)
	res[3] = "StartDate: " + reform.Inspect(s.StartDate, true)
	res[4] = "LastSeenDate: " + reform.Inspect(s.LastSeenDate, true)
	return strings.Join(res, ", ")
}

// Values returns a slice of struct or record field values.
// Returned interface{} values are never untyped nils.
func (s *Link) Values() []interface{} {
	return []interface{}{
		s.ShortCode,
		s.URL,
		s.RedirectCount,
		s.StartDate,
		s.LastSeenDate,
	}
}

// Pointers returns a slice of pointers to struct or record fields.
// Returned interface{} values are never untyped nils.
func (s *Link) Pointers() []interface{} {
	return []interface{}{
		&s.ShortCode,
		&s.URL,
		&s.RedirectCount,
		&s.StartDate,
		&s.LastSeenDate,
	}
}

// View returns View object for that struct.
func (s *Link) View() reform.View {
	return LinkTable
}

// Table returns Table object for that record.
func (s *Link) Table() reform.Table {
	return LinkTable
}

// PKValue returns a value of primary key for that record.
// Returned interface{} value is never untyped nil.
func (s *Link) PKValue() interface{} {
	return s.ShortCode
}

// PKPointer returns a pointer to primary key field for that record.
// Returned interface{} value is never untyped nil.
func (s *Link) PKPointer() interface{} {
	return &s.ShortCode
}

// HasPK returns true if record has non-zero primary key set, false otherwise.
func (s *Link) HasPK() bool {
	return s.ShortCode != LinkTable.z[LinkTable.s.PKFieldIndex]
}

// SetPK sets record primary key.
func (s *Link) SetPK(pk interface{}) {
	if i64, ok := pk.(int64); ok {
		s.ShortCode = string(i64)
	} else {
		s.ShortCode = pk.(string)
	}
}

// check interfaces
var (
	_ reform.View   = LinkTable
	_ reform.Struct = (*Link)(nil)
	_ reform.Table  = LinkTable
	_ reform.Record = (*Link)(nil)
	_ fmt.Stringer  = (*Link)(nil)
)

func init() {
	parse.AssertUpToDate(&LinkTable.s, new(Link))
}
