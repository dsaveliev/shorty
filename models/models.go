package models

import (
	"math/rand"
	"regexp"
	"time"

	"gitlab.com/dsaveliev/shorty/errors"
)

const (
	shortCodeBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_"
	defaultLength  = 6
)

var (
	emptyTimeStamp time.Time
	validShortCode = regexp.MustCompile(`^[0-9a-zA-Z_]{4,}$`)
)

// Link describes link instance
//go:generate reform
//reform:link
type Link struct {
	ShortCode     string    `reform:"shortcode,pk"`
	URL           string    `reform:"url"`
	RedirectCount uint64    `reform:"redirect_count"`
	StartDate     time.Time `reform:"start_date"`
	LastSeenDate  time.Time `reform:"last_seen_date"`
}

// ShortCodePresenter represents shortcode response
func (l *Link) ShortCodePresenter() map[string]interface{} {
	return map[string]interface{}{
		"shortcode": l.ShortCode,
	}
}

// StatsPresenter represents stats response
func (l *Link) StatsPresenter() map[string]interface{} {
	return map[string]interface{}{
		"startDate":     l.StartDate,
		"lastSeenDate":  l.LastSeenDate,
		"redirectCount": l.RedirectCount,
	}
}

// Validate checks all the link's fields
func (l *Link) Validate() error {
	if l.URL == "" {
		return errors.ErrorEmptyURL
	}
	if l.ShortCode == "" {
		l.ShortCode = generateShortCode()
	} else if !validShortCode.MatchString(l.ShortCode) {
		return errors.ErrorWrongShortCodeFormat
	}
	if l.StartDate == emptyTimeStamp {
		l.StartDate = time.Now()
	}
	if l.LastSeenDate == emptyTimeStamp {
		l.LastSeenDate = time.Now()
	}
	return nil
}

// UpdateStats method update counter and timestamps
func (l *Link) UpdateStats() {
	l.LastSeenDate = time.Now()
	l.RedirectCount++
}

func generateShortCode() string {
	b := make([]byte, defaultLength)
	for i := range b {
		b[i] = shortCodeBytes[rand.Intn(len(shortCodeBytes))]
	}
	return string(b)
}
