package models

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/dsaveliev/shorty/errors"
)

var validGeneratedShortCode = regexp.MustCompile(`^[0-9a-zA-Z_]{6}$`)

func TestShortCodePresenter(t *testing.T) {
	link := Link{
		URL:       "http://example.com",
		ShortCode: "123456",
	}
	expected := map[string]interface{}{
		"shortcode": "123456",
	}

	assert.Equal(t, link.ShortCodePresenter(), expected)
}

func TestStatsPresenter(t *testing.T) {
	link := Link{
		URL:       "http://example.com",
		ShortCode: "123456",
	}
	link.Validate()
	expected := map[string]interface{}{
		"startDate":     link.StartDate,
		"lastSeenDate":  link.LastSeenDate,
		"redirectCount": link.RedirectCount,
	}

	assert.Equal(t, link.StatsPresenter(), expected)
}

func TestStatsValidate(t *testing.T) {
	link := Link{
		URL: "http://example.com",
	}
	link.Validate()

	assert.NotZero(t, link.ShortCode)
	assert.True(t, validGeneratedShortCode.MatchString(link.ShortCode))
	assert.NotZero(t, link.StartDate)
	assert.NotZero(t, link.LastSeenDate)
	assert.Equal(t, uint64(0), link.RedirectCount)
}

func TestStatsValidateErrors(t *testing.T) {
	link := Link{
		URL: "",
	}
	assert.Equal(t, link.Validate(), errors.ErrorEmptyURL)

	link = Link{
		URL:       "http://example.com",
		ShortCode: "123$%^567",
	}
	assert.Equal(t, link.Validate(), errors.ErrorWrongShortCodeFormat)
}

func TestUpdateStats(t *testing.T) {
	link := Link{
		URL: "http://example.com",
	}

	link.Validate()
	lastSeenDate := link.LastSeenDate
	assert.Equal(t, uint64(0), link.RedirectCount)

	link.UpdateStats()
	assert.Equal(t, uint64(1), link.RedirectCount)
	assert.True(t, lastSeenDate.Before(link.LastSeenDate))
}
